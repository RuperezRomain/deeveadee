var URL = "http://deeveadee.my/";
var table = null;
var CONTENT = $("#result");

$().ready(function () {
    getDvds();

});
function getDvds() {
    $.ajax({
        url: URL + "dvd/getDvd",
        async: true,
        type: "GET",
        datatype: "json",
        success: function (data) {
            createTableDvds(data);
        },
        error: function () {
            alert("problème");
        }
    });

}

//Fabrique le rendu (<table>) et l'insere dans la zone prévue (<div id="content"></div>)
//L'algorythme est découpé en 3 parties 
function createTableDvds(dvds) {
    //prepration de la balise table
    table = document.createElement("table");
    $("#result").append(table);
    //affectation des lignes
    $(table).append(getHeaderLineForTableDvd());
    generateContentForTableDvd(dvds);
    //mise en forme via l'application de classe css
    $(table).addClass("col-xs-10 col-xs-offset-1 col-lg-10");
//    table.setAttribute("id", "dvds");
//  document.getElementById('dvds').appendChild(
//    document.getElementById('filter')
//  );    
    
    
}

function generateContentForTableDvd(dvds) {
var trmi = document.createElement('tr');
var td1 = document.createElement('td');
var td2 = document.createElement('td');
var td3 = document.createElement('td');
var td4 = document.createElement('td');
var td5 = document.createElement('td');
//$(trmi).addClass("container-fluid");
var img = document.createElement("input");
img.setAttribute('type', 'text');
img.setAttribute('placeholder', 'Image');
$(img).addClass("col-lg-12");
td1.appendChild(img);
trmi.append(td1);

var title = document.createElement("input");
title.setAttribute('type', 'text');
title.setAttribute('placeholder', 'Titre');
$(title).addClass("col-lg-12");
td2.appendChild(title);
trmi.append(td2);

var genre = document.createElement("input");
genre.setAttribute('type', 'text');
genre.setAttribute('placeholder', 'Genre');
$(genre).addClass("col-lg-12");
td3.appendChild(genre);
trmi.append(td3);

var annee = document.createElement("input");
annee.setAttribute('type', 'text');
annee.setAttribute('placeholder', 'Année');
$(annee).addClass("col-lg-12");
td4.appendChild(annee);
trmi.append(td4);

var director = document.createElement("input");
director.setAttribute('type', 'text');
director.setAttribute('placeholder', 'default');
$(director).addClass("col-lg-12");
td5.appendChild(director);
trmi.append(td5);

        $(table).append(trmi);
    $.each(dvds, function () {
        $(table).append(getLineForTableDvd(this));
    });

}

function getHeaderLineForTableDvd() {
    //creation des balises
    var ligne = document.createElement("tr");
    var imgDvd = document.createElement("th");
    var titleDvd = document.createElement("th");
    var director = document.createElement("th");
    var societe = document.createElement("th");
    var anneeSortie = document.createElement("th");
    var nbExemplaire = document.createElement("th");

    //affectation des textes
    $(imgDvd).text("Image");
    $(titleDvd).text("Titre");
    $(director).text("Réalisateur");
    $(societe).text("Société");
    $(anneeSortie).text("Année de Sortie");
    $(nbExemplaire).text("Nbr Exemplaires");

    //insertion des th dans la ligne
//    $(ligne).addClass('thead-default');
    $(ligne).append(imgDvd);
    $(ligne).append(titleDvd);
    $(ligne).append(director);
    $(ligne).append(societe);
    $(ligne).append(anneeSortie);
    $(ligne).append(nbExemplaire);
        ligne.setAttribute("id", "thead");

    return ligne;
}

function getLineForTableDvd(dvd) {
    var lineDvd = document.createElement("tr");

    var imgDvd = document.createElement("td");
    var titleDvd = document.createElement("td");
    var directorDvd = document.createElement("td");
    var societe = document.createElement("td");
    var anneeSortie = document.createElement("td");
    var nbExemplaire = document.createElement("td");

    var imgDvdSrc = document.createElement("img");
    imgDvdSrc.src = "/source/ImagesDvd/" + dvd.img;
    $(imgDvdSrc).addClass("img-thumbnail");
    $(imgDvdSrc).attr("width", "100");

    $(titleDvd).text(dvd.titre);
    $(directorDvd).text(dvd.realisateur);
    $(societe).text(dvd.societe_distribution);
    $(anneeSortie).text(dvd.annee_sortie);
    $(nbExemplaire).text(dvd.nbr_exemplaire);

    lineDvd.setAttribute("id", dvd.id_dvd);
    $(lineDvd).append(imgDvd);
    $(imgDvd).append(imgDvdSrc);
    $(lineDvd).append(titleDvd);
    $(lineDvd).append(directorDvd);
    $(lineDvd).append(societe);
    $(lineDvd).append(anneeSortie);
    $(lineDvd).append(nbExemplaire);

/* Redirect at the dvd single page*/
    $(lineDvd).click(function () {
        window.location.href = "dvd/" + dvd.id_dvd;
    });
    return lineDvd;

//    ("id : " + book.id + "- titre :" + book.title + "- auteur :" + book.author);

}